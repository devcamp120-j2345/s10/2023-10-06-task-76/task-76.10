package com.devcamp120.task76.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="customers")
public class Customer {

    @Id
    @GeneratedValue()
    private int id;

    @Column(name="last_name", columnDefinition = "varchar(50)")
    private String lastName;

    @Column(name="first_name", columnDefinition = "varchar(50)")
    private String firstName;

    @Column(name="phone_number", columnDefinition = "varchar(50)")
    private String phoneNumber;

    @Column(name="address", columnDefinition = "varchar(255)")
    private String address;

    @Column(name="city", columnDefinition = "varchar(50)")
    private String city;

    @Column(name="state", columnDefinition = "varchar(50)")
    private String state;

    @Column(name="postal_code", columnDefinition = "varchar(50)")
    private String postalCode;

    @Column(name="country", columnDefinition = "varchar(50)")
    private String country;

    @Column(name="sales_rep_employee_number", columnDefinition = "int(10)")
    private int salesRepEmployeeNumber;

    @Column(name="credit_limit", columnDefinition = "int(10)")
    private int creditLimit;

    @OneToMany(mappedBy = "customer")
    private List<Payment> payments;

    @OneToMany(mappedBy = "customer")
    private List<Order> orders;
}
