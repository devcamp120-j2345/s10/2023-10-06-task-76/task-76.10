package com.devcamp120.task76.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="order_details")
public class OrderDetail {
    @Id
    @GeneratedValue
    private int id;

    @Column(name="quantity_order", columnDefinition = "int(10)")
    private int quantityOrder;

    @Column(name="price_each", columnDefinition = "Decimal(10,2)")
    private double priceEach;
    
    @ManyToOne
    @JoinColumn(name="order_id")
    private Order order;
}
