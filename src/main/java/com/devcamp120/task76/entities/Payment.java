package com.devcamp120.task76.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "payments")
public class Payment {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "check_number", columnDefinition = "varchar(50)")
    private String checkNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "payment_date")
    private Date paymentDate;

    @Column(name = "amount", columnDefinition = "Decimal(10,2)")
    private double amount;

    @ManyToOne()
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
