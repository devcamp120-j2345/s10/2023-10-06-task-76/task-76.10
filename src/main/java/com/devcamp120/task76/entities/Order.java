package com.devcamp120.task76.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="orders")
public class Order {

    @Id
    @GeneratedValue()
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="order_date")
    private Date orderDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="required_date")
    private Date requiredDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="shipped_date")
    private Date shippedDate;

    @Column(name="status", columnDefinition = "varchar(50)")
    private String status;

    @Column(name="comments", columnDefinition = "varchar(255)")
    private String comments;
  
    @ManyToOne
    @JoinColumn(name="customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "order")
    private List<OrderDetail> orderDetails;

}
