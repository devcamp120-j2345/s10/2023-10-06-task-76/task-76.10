package com.devcamp120.task76;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task76Application {

	public static void main(String[] args) {
		SpringApplication.run(Task76Application.class, args);
	}

}
